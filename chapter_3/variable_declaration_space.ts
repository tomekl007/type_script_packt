class E {};
var someVar = E;
var someOtherVar = 123;

var foo = 123;
var bar: E; // ERROR: "cannot find name 'foo'"